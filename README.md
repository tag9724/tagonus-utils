# Tagonus-Utils

This project contain a list of utility functions and classes.

## Usage

```javascript
import { genUID, number3Format } from "index.mjs";
```

Feel free to navigate inside the `modules` folder, all util functions/class are documented with a header comment.
