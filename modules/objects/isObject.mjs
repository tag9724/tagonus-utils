/**
 * @summary
 * Check the validity of a js object, excluding array and null type
 * of variables as a valid result
 *
 * @example
 *
 * // Will return false
 * isObject( [] );
 * isObject( null );
 * isObject( /regexp/g );
 *
 * // Will return true
 * isObject( {} );
 * isObject( { a:()=>{} } );
 */

export const isObject = obj => {
  return (
    obj !== null &&
    typeof obj === "object" &&
    !Array.isArray(obj) &&
    !(obj instanceof RegExp)
  );
};
