/**
 * @summary
 *   Perform a effciency loop over an object
 *
 * @example
 *
 *   const obj = { a:1, b:2 };
 *   loopKeys( obj, ( key ) => console.log( key, obj[key] ) );
 *
 *   // You can also return a array from it
 *   const list = loopKeys( obj, ( key ) => obj[key] );
 *   console.log(list); // [ 1, 2 ];
 */

export const loopKeys = (object = {}, callback = () => {}) => {
  return Object.keys(object).map(callback);
};
