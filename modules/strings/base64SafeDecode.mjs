/** 
 * @summary
 *   Use regular atob function but with proper error handling.
 * 
 * @example
 *   base64SafeDecode();             // return ""
 *   base64SafeDecode( null );       // return false
 * 
 *   base64SafeDecode( "aGVsbG8=" ); // return "hello"
*/

export const base64SafeDecode = (base64String = "") => {
  if (typeof base64String !== "string") {
    return false;
  }

  try {
    return atob(base64String);
  } catch (error) {
    return false;
  }
};
