/** 
 * @summary
 *   Dummy function for escaping string.
 * 
 * @example
 * 
 *   // "&lt;script&gt;Malicious code&lt;&#x2F;script&gt;"
 *   escapeHTML( "<script>Malicious code</script>" );
 * 
 *   // "123456"
 *   escapeHTML( 123456 );
*/

export const escapeHTML = (string = "") => {
  return String(string).replace(/[&<>"'`=/]/g, s => entityMap[s]);
};

/* 
  The list of entities chars used by the function above
*/

const entityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': "&quot;",
  "'": "&#39;",
  "/": "&#x2F;",
  "`": "&#x60;",
  "=": "&#x3D;"
};
