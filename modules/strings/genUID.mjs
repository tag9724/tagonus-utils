/**
 * @summary
 *   Gen fixed lenght UID with timestamp included,
 *   recommended to use 12 as minimum length.
 *
 * @example
 *   genUID()     // "MqVUblBbpoBeRGKfAqLjpknrX"
 *   genUID( 12 ) // "gwcVblBbChgO"
 *   genUID( 1 )  // "Q"
 */

import { alphabetEncode } from "./alphabetEncode.mjs";

export const genUID = (length = 25) => {
  let result = alphabetEncode(Date.now());
  const b52Length = result.length;

  for (let i = b52Length; i < length; i++) {
    const range = Math.random() > 0.5 ? 65 : 97;
    result += String.fromCharCode(range + Math.floor(Math.random() * 26));
  }

  return result.slice(0, length);
};
