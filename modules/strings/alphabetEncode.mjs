/**
 * @summary
 *  Will encode a number to a base 52 string containing only lower case
 *  and uppercase letters ( no digits )
 *
 * @example
 *  alphabetEncode( 123456789 ) // "bfbUq"
 */

const alphabetMap = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

export const alphabetEncode = (number = 0) => {
  let elements = "";
  let value = number;

  while (value > 0) {
    elements += alphabetMap.charAt(value % 52);
    value = Math.floor(value / 52);
  }

  return elements || "a";
};
