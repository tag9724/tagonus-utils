/**
 * @summary
 *  Uppercase the very first caracter of a string
 *
 * @example
 *
 * capitalize() // ""
 * capitalize("hello world") // "Hello world"
 * capitalize(" hello") // " hello"
 */

export const capitalize = (str = "") => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};
