/**
 * @summary
 *  Convert an amount of seconds to a proper human
 *  readable countdown in days/minutes.
 *
 * @example
 *
 * const calc = new CountdownCalculator({ minute: " minute", day: "j" });
 *
 * calc.parseSeconds( 86399 ); // "23h"
 * calc.parseSeconds( 80 );    // "1 minute"
 * calc.parseSeconds( 380 );   // "6 min"
 *
 * calc.parseMilliseconds( 265000000 ); // "3j"
 */

export class CountdownCalculator {
  constructor(units = {}) {
    this.units = {
      year: "y",
      years: "y",
      month: "m",
      months: "m",
      day: "d",
      days: "d",
      hour: "h",
      hours: "h",
      minute: "min",
      minutes: "min",
      ...units
    };
  }

  parseDate(date = new Date()) {
    return this.parseSeconds((date.getTime() - Date.now()) / 1000);
  }

  parseMilliseconds(milliseconds = 0) {
    return this.parseSeconds(milliseconds / 1000);
  }

  parseSeconds(seconds = 0) {
    const parsed = this.getSecondsUnitAndValue(seconds);

    if (parsed.value > 1) {
      return parsed.value + this.units[`${parsed.unit}s`];
    }

    return parsed.value + this.units[parsed.unit];
  }

  getSecondsUnitAndValue(seconds = 0) {
    // More than 12 months
    if (seconds >= 31536e3) {
      return { unit: "year", value: Math.floor(seconds / 31536e3) };
    }

    // More than 30 days
    if (seconds >= 2592e3) {
      return { unit: "month", value: Math.floor(seconds / 2592e3) };
    }

    // More than 24 hours
    if (seconds >= 86400) {
      return { unit: "day", value: Math.floor(seconds / 86400) };
    }

    // More than 60 minutes
    if (seconds >= 3600) {
      return { unit: "hour", value: Math.floor(seconds / 3600) };
    }

    // Less than 60 minutes
    return {
      unit: "minute",
      value: Math.max(1, Math.floor((seconds / 60) % 60))
    };
  }
}
