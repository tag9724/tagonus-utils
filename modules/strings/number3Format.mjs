/**
 * @summary
 *   Turn Number into string formated by 3 ( exemple : 1 123 456 ).
 *   Can also fix the amount max of decimals with the optional second param.
 *
 * @example
 *   number3Format( 56000 )      // "56 000"
 *   number3Format( 1200.29, 2 ) // "1 200.29"
 *
 *   number3Format( 1e6, 2 )    // "1 000 000.00"
 *   number3Format( 4500.985 )  // "4 500"
 */

export const number3Format = (nb = 0, fix = 0) => {
  return Number(nb)
    .toFixed(fix)
    .replace(/\d(?=(\d{3})+($|[.]))|([.].*)/g, "$& ")
    .replace(/[ ]$/, "");
};
