/**
 * @summary
 * Will take a number and return power of 10 value of the number length
 *
 * @example
 * numberPowLength( 23 );  // 100
 * numberPowLength( 135 ); // 1000
 */

export const numberPowLength = (nb = 0) => 10 ** nb.toString().length;
