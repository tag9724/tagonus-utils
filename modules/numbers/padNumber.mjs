/**
 * @summary
 * Take a number and add zeros in front of it
 *
 * @description
 * The second parameter will define how long the resulted string
 * will be, in other case of many zeros must be included to fill the
 * string.
 *
 * @example
 * padNumber( 9, 2 )   // return 09
 * padNumber( 123, 2 ) // return 23
 * padNumber( 123, 4 ) // return 0123
 */

export const padNumber = (nb = 0, range = 0) => {
  return `${10 ** range + nb}`.slice(-range);
};
