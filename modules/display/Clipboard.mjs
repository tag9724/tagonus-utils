/**
 * @summary
 *   Allow you to manage user clipboard.
 *
 * @example
 *   // Copy from regular string
 *   clipboard.copyToClipboardFromText( "Hello world" );
 *
 *   // Copy from event & input
 *   clipboard.addElementCopyTriggerToInput( button, input, copyEvent );
 */

export class Clipboard {
  init() {
    this.input = this.createInput();
  }

  createInput() {
    const input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("readonly", "true");

    return input;
  }

  /* Perform copy from string */

  copyToClipboardFromText(text = "") {
    // Create the text container
    if (!this.input) {
      this.input = this.createInput();
    }

    // Set input content and append it
    this.input.value = text;
    document.body.append(this.input);

    // Unselect everything on the page
    window.getSelection().removeAllRanges();

    // Select & make copy
    this.input.select();
    document.execCommand("copy");

    // Remove the container
    document.body.removeChild(this.input);
  }

  /* Copy from input element in the page */

  copyToClipboardFromSourceInput(input) {
    // Prevent Unselection
    window.getSelection().removeAllRanges();

    // Focus the input element
    input.select();

    // And perform copy
    document.execCommand("copy");
  }

  addElementCopyTriggerToInput(element, input, callback = () => {}) {
    element.onclick = () => {
      this.copyToClipboardFromSourceInput(input);
      callback();
    };
  }
}
