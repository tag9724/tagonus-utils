/**
 * @summary
 *   Take a DOM Node element as parameter and return a JSON object
 *   with all input values.
 */

export const formDataToJSON = formElement => {
  const result = {};
  const inputs = formElement.querySelectorAll("input[name]");

  for (let i = 0, len = inputs.length; i < len; i++) {
    result[inputs[i].getAttribute("name")] = inputs[i].value;
  }

  return result;
};
