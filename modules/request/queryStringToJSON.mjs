/**
 * @summary
 *  Will return a json of GET params from url string
 *
 * @example
 *
 *  // All examples above return { uid: "378291", pos:"42" }
 *  queryStringToJSON( "https://example.com/?&uid=378291&pos=42" ) ;
 *
 *  queryStringToJSON( "example.com/?&uid=378291&pos=42" ) ;
 *  queryStringToJSON( "example.com&uid=378291&pos=42" ) ;
 *
 *  queryStringToJSON( "?&uid=378291&pos=42" );
 *  queryStringToJSON( "&uid=378291&pos=42" );
 *  queryStringToJSON( "uid=378291&pos=42" );
 */

export const queryStringToJSON = (url = "") => {
  const pairs = url.replace(/.+\?/, "").split("&");
  const pairsLength = pairs.length;
  const result = {};

  for (let i = url.charAt(0) === "&" ? 1 : 0; i < pairsLength; i++) {
    const pair = pairs[i].split("=");
    result[pair[0]] = decodeURIComponent(pair[1] || "");
  }

  return result;
};
