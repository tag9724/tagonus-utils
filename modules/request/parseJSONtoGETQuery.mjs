/**
 * @summary
 *  Transform an object to a proper GET url query
 *
 * @example
 *
 * parseJSONtoGETQuery( "rank", { user: "Paul" } ) // return "rank?user=Paul"
 */

export const parseJSONtoGETQuery = (route = "", body = {}) => {
  const urlParams = new URLSearchParams(body);
  return `${route}?${urlParams.toString()}`;
};
