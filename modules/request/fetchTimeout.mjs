/**
 * @summary
 *  Perform a regular fetch request with a additionnal timeout argument
 *
 * @example
 *
 * const data = await fetchTimeout( "/api/status", { method: "GET" }, 1000 );
 * // data = { status: 408 } // If timeout occur
 */

export const fetchTimeout = async (url = "", body = {}, timeout = 5000) => {
  const controller = new AbortController();
  const { signal } = controller;

  const request = fetch(url, { ...body, signal });
  const timeoutId = setTimeout(() => controller.abort(), timeout);

  try {
    const data = await request;
    clearTimeout(timeoutId);
    return data;
  } catch (error) {
    clearTimeout(timeoutId);
    return { status: 408 };
  }
};
