/**
 * @summary
 *   Create a blocking timeout.
 * 
 * @example
 *   await sleep( 2000 ); // Wait 2 seconds
*/

export const sleep = (milliseconds = 0) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
};
