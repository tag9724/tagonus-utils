/**
 * @summary
 *  Debouncer for promise, will prevent calling
 *  multiple functions when waiting for a promise to end.
 *
 * @description
 *  When you want to trigger a function, possibly multiples time this debouncer
 *  will assure you to only execute the last one when the promise set is done.
 *
 * @example
 *
 * // First thing setup the debouncer :
 * const debouncer = new DebouncePromise();
 *
 * // When no promise is pending no debounce is needed
 * debouncer.run( myFunction1 ); // Will be executed
 * debouncer.run( myFunction2 ); // Will be executed
 *
 * // It's a different story when you have a pending one
 * debouncer.set( sleep( 2000 ) );
 *
 * // Now for the next 2sec ...
 * debouncer.run( myFunction1 ); // Will be ignored
 * debouncer.run( myFunction2 ); // Will be ignored
 * debouncer.run( myFunction3 ); // Will be executed after 2000ms
 */

import { EventMaker } from "../events/EventMaker.mjs";

export class DebouncePromise {
  constructor() {
    this.isDone = true;

    this.evmOnDone = new EventMaker();
    this.evmOnChange = new EventMaker();
  }

  set(promise = {}) {
    this.current = promise;
    this.isDone = false;

    // Reject previous Promise
    if (this.running) {
      this.evmOnChange.dispatch();
    }

    // Init the new running one
    this.running = this.wait(promise);
  }

  async wait(promise = {}) {
    let rejected = false;

    const listener = this.evmOnChange.addListener(() => {
      rejected = true;
      listener.remove();
    });

    await promise;

    if (!rejected) {
      this.isDone = true;

      if (this.currentCallback) {
        this.currentCallback();
        this.currentCallback = null;
      }
    }
  }

  run(callback = () => {}) {
    if (this.isDone) {
      callback();
    }

    // Wait for running promise to end
    else {
      this.currentCallback = callback;
    }
  }
}
