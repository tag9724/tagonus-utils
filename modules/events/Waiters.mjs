/**
 * @summary
 * Task will register themself as Waiters, when they have done doing
 * they tasks they will fire a onFinish Event.
 *
 * Note : onFinish Must be declared after all register to
 *        prevent false positives
 *
 * @example
 *
 * const waiters = new Waiters();
 *
 * const waiter1 = waiters.register();
 * const waiter2 = waiters.register();
 *
 * setTimeout( () => waiter1.finish(), 5000 );
 * setTimeout( () => waiter2.finish(), 1000 );
 *
 * waiters.onFinish( () => {} ); // Will be fired after 5000ms
 */

import { EventMaker } from "./EventMaker.mjs";

export class Waiters {
  constructor() {
    this.evmFinished = new EventMaker();
    this.evmTicked = new EventMaker();
    this.evmUpdate = new EventMaker();

    this.isFinished = false;
    this.queue = 0;
    this.loaded = 0;
  }

  register() {
    this.isFinished = false;
    this.queue++;

    let finished = false;
    this.evmUpdate.dispatch();

    const finish = () => {
      if (!finished) {
        finished = true;
        this._finishedTask();
      }
    };

    return { finish };
  }

  /* 
    Fired everytime a task is added or done 
    return ( loaded, total )
  */

  onUpdate(callback = () => {}) {
    callback(this.loaded, this.queue + this.loaded);

    this.evmUpdate.addListener(() => {
      callback(this.loaded, this.queue + this.loaded);
    });
  }

  /* 
    Fired when a task is done
    return ( loaded, total )
  */

  onTick(callback = () => {}) {
    callback(this.loaded, this.queue + this.loaded);

    this.evmTicked.addListener(() => {
      callback(this.loaded, this.queue + this.loaded);
    });
  }

  /* 
    Fired when all tasks are done
    return ( total )
  */

  onFinish(callback = () => {}) {
    if (this.isFinished) {
      callback(this.queue + this.loaded);
    }

    this.evmFinished.addListener(() => {
      callback(this.queue + this.loaded);
    });
  }

  _finishedTask() {
    this.queue--;
    this.loaded++;

    this.evmTicked.dispatch();
    this.evmUpdate.dispatch();

    // All pending tasks are done
    if (this.queue < 1) {
      this.isFinished = true;
      this.evmFinished.dispatch();
    }
  }
}
