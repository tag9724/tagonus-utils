/**
 * @summary
 *  Simple debounce function.
 *
 * @example
 *
 * const deb1 = debouncer( (a,b) => console.log(a,b), 1000 );
 * deb1(1,2) // will console.log( 1, undefined );
 *
 * const deb2 = debouncer( (a) => console.log(a), 1000 );
 *
 * deb2(1) // Executed imediatly
 * deb2(2) // Will be executed in 1000ms
 * deb2(3) // Ignored
 * deb2(4) // Ignored
 *
 */

export const debouncer = (callback, interval = 1000) => {
  let timeout;
  let lastCheck = 0;

  return (ev) => {
    if (!timeout) {
      const dateNow = Date.now();
      const inteNow = dateNow - interval;

      // Imediate callback
      if (lastCheck < inteNow) {
        lastCheck = dateNow;
        callback(ev);
      }

      // Delayed callback
      else {
        timeout = true;
        setTimeout(() => {
          timeout = false;
          callback(ev);
        }, dateNow - inteNow);
      }
    }
  };
};
