/**
 * @summary
 *   Simple interface for handling keyboard shortcuts
 *
 * @description
 *   format is the code of a keyboard key
 *   "altLeft+Digit1"
 *
 * @example
 *   const keypress = new KeyPressEvents();
 *
 *   keypress.registerShortcut( "altLeft+KeyQ", () => {
 *     // Will be fired every time a user perform alt+a shortcut
 *   });
 *
 *   // Stop listening keyboard
 *   keypress.stop();
 */

export class KeyPressEvents {
  constructor(container = window) {
    this.container = container;

    this.activeKeys = [];
    this.shortcutListeners = [];

    // Store events for removeEventListener
    this.events = {
      down: ev => this.handleDownEvent(ev),
      up: ev => this.handleUpEvent(ev)
    };

    // Prevent bad keyup handling
    window.addEventListener("blur", () => {
      this.activeKeys = [];
    });

    // Start listening keyboard
    this.start();
  }

  /* 
    Enable or disable events listener on container
  */

  start() {
    if (this.activeState) return;
    this.activeState = true;

    this.container.addEventListener("keydown", this.events.down);
    this.container.addEventListener("keyup", this.events.up);
  }

  stop() {
    if (this.activeState) {
      this.container.removeEventListener("keydown", this.events.down);
      this.container.removeEventListener("keyup", this.events.up);
    }

    this.activeState = false;
    this.activeKeys = [];
  }

  toggle(toggle = true) {
    toggle ? this.start() : this.stop();
  }

  /* 
    Add a event when a shortcut is performed
  */

  registerShortcut(shortcut = "", callback = () => {}) {
    this.shortcutListeners.push({
      shortcut: shortcut.toLowerCase(),
      triggerEvent: callback
    });
  }

  /* 
    Take action when keys are pressed or released
  */

  handleDownEvent(ev) {
    if (!ev.code) return;
    const code = ev.code.toLowerCase();

    if (!this.activeKeys.includes(code)) {
      this.activeKeys.push(code);

      // Now check if currently active keys make a shortcut
      const currentShortcut = this.activeKeys.join("+");

      for (const short of this.shortcutListeners) {
        if (short.shortcut === currentShortcut) {
          short.triggerEvent();
          ev.preventDefault();
        }
      }
    }
  }

  handleUpEvent(ev) {
    if (ev.code) {
      const code = ev.code.toLowerCase();
      this.activeKeys = this.activeKeys.filter(e => e !== code);
    }
  }
}
