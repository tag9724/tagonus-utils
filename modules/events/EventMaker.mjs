/**
 * @summary
 *  Allow you to create a custom event way easely
 *
 * @example
 *  const evm = new EventMaker();
 *
 *  // Will be fired every time a dispatch happend
 *  const listener = evm.addListener( (a,b,c) => console.log( a,b,c ) );
 *
 *  // Will be dispatched only if the first dispatched happend or will happend
 *  evm.addTrigger( (a,b,c) => console.log( a,b,c ) );
 *
 *  // The previous addListener will be trigger 2 time
 *  // addTrigger only one time
 *  evm.dispatch( 1,2,3 );
 *  evm.dispatch( 1,2,3 );
 *
 *  // Will callback imediatly since the event was dispatched once
 *  evm.addTrigger( (a,b,c) => console.log( a,b,c ) );
 *
 *  evm.getListenedAmount() // return 2
 *  evm.isDispatchedOnce()  // return true
 *
 *  // Destroy the listener
 *  listener.remove()
 */

import { genUID } from "../strings/genUID.mjs";

export class EventMaker {
  constructor() {
    this.UID = `EventMaker-${genUID(20)}`;
    this.element = document.createElement("var");

    this.dispatched = 0;
    this.listened = 0;
  }

  addListener(callback) {
    // Setup event callback
    const evCallback = (ev) => {
      callback(...ev.detail);
      this.listened++;
    };

    // Add event listener
    this.element.addEventListener(this.UID, evCallback);

    // Return a way to destroy this listener
    const remove = () => this.element.removeEventListener(this.UID, evCallback);
    return { remove };
  }

  addTrigger(callback) {
    if (this.dispatched > 0) {
      callback(...this.lastDispatchData);
      return { remove: () => {} };
    }

    // Event was not yet dispatched
    else {
      const listener = this.addListener(() => {
        callback(...this.lastDispatchData);
        listener.remove();
      });

      return listener;
    }
  }

  dispatch(...args) {
    const event = new CustomEvent(this.UID, { detail: args });
    this.lastDispatchData = args;
    this.element.dispatchEvent(event);

    this.dispatched++;
  }

  isDispatchedOnce() {
    return this.dispatched > 0;
  }

  isListenedOnce() {
    return this.listened > 0;
  }

  getDispatchedAmount() {
    return this.dispatched;
  }

  getListenedAmount() {
    return this.dispatched;
  }
}
