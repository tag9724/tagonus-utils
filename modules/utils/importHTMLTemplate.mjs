/**
 * @summary
 *   Async import of HTML template file
 * 
 * @example
 *   const element = await importHTMLTemplate( "/html/button.html");
 *   document.body.append( element );
*/

export const importHTMLTemplate = async (file = "") => {
  const result = await fetch(file).then(result => result.text());

  // Create DOM Parser container
  const element = document.createElement("template");
  element.innerHTML = result;

  // Return template element
  return element.content.children[0];
};
